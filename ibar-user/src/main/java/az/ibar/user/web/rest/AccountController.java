package az.ibar.user.web.rest;


import az.ibar.user.dto.AccountDto;
import az.ibar.user.dto.AccountRequestDto;
import az.ibar.user.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ibar/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public AccountDto create(@RequestBody AccountRequestDto accountDto){
        return accountService.create(accountDto);
    }

    @GetMapping("/{id}")
    public AccountDto get(@PathVariable Long id){
        return accountService.get(id);
    }

    @PutMapping("/{id}")
    public AccountDto update(@PathVariable Long id, @RequestBody AccountRequestDto accountDto){
        return accountService.update(id,accountDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        accountService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
