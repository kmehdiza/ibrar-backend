package az.ibar.user.service;

import az.ibar.user.dto.UserDto;
import az.ibar.user.dto.UserRequestDto;

public interface UserService {
    UserDto create(UserRequestDto userDto);

    UserDto get(Long id);

    UserDto update(Long id, UserRequestDto userDto);

    void delete(Long id);
}
