package az.ibar.user.service.impl;

import az.ibar.user.common.NotFoundException;
import az.ibar.user.domain.Account;
import az.ibar.user.domain.User;
import az.ibar.user.dto.AccountDto;
import az.ibar.user.dto.AccountRequestDto;
import az.ibar.user.exception.AccountNotFoundException;
import az.ibar.user.exception.UserNotFoundException;
import az.ibar.user.repository.AccountRepository;
import az.ibar.user.repository.UserRepository;
import az.ibar.user.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public AccountDto create(AccountRequestDto accountDto) {
        Account account = modelMapper.map(accountDto, Account.class);
        User user = findUserById(accountDto.getUserId());
        account.setUser(user);
        account.setAccountBalance(0.0);
        return modelMapper.map(accountRepository.save(account), AccountDto.class);
    }

    @Override
    public AccountDto get(Long id) {
        return accountRepository.findById(id)
                .map(account -> modelMapper.map(account, AccountDto.class))
                .orElseThrow(() -> new AccountNotFoundException(id));
    }

    @Override
    public AccountDto update(Long id, AccountRequestDto accountDto) {
        return accountRepository.findById(id).map(account -> {
            modelMapper.map(accountDto, account);
            accountRepository.save(account);
            return modelMapper.map(account, AccountDto.class);
        }).orElseThrow(() -> new AccountNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        try {
            accountRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format("Entry with id %s not found", id), ex);
        }
    }

    private User findUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }
}
