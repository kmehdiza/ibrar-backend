package az.ibar.user.service;

import az.ibar.user.dto.AccountDto;
import az.ibar.user.dto.AccountRequestDto;

public interface AccountService {
    AccountDto create(AccountRequestDto accountDto);

    AccountDto get(Long id);

    AccountDto update(Long id, AccountRequestDto accountDto);

    void delete(Long id);
}
