package az.ibar.user.service.impl;

import az.ibar.user.common.NotFoundException;
import az.ibar.user.domain.User;
import az.ibar.user.dto.UserDto;
import az.ibar.user.dto.UserRequestDto;
import az.ibar.user.exception.UserNotFoundException;
import az.ibar.user.repository.UserRepository;
import az.ibar.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public UserDto create(UserRequestDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        user.setCreated(LocalDateTime.now());
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public UserDto get(Long id) {
        return userRepository.findById(id).map(user -> {
            return modelMapper.map(user, UserDto.class);
        }).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public UserDto update(Long id, UserRequestDto userDto) {
        return userRepository.findById(id).map(user -> {
            modelMapper.map(userDto, user);
            userRepository.save(user);
            return modelMapper.map(user, UserDto.class);
        }).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format("Entry with id %s not found", id), ex);
        }
    }
}
