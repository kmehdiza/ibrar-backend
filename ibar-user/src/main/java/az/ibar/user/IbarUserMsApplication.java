package az.ibar.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IbarUserMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(IbarUserMsApplication.class, args);
    }
}
