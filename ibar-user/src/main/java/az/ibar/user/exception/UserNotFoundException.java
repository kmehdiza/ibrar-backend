package az.ibar.user.exception;

import az.ibar.user.common.NotFoundException;

public class UserNotFoundException extends NotFoundException {
    private static final long serialVersionUID = 58432132487812L;

    public static final String MESSAGE = "User Id %s does not exist.";

    public UserNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
